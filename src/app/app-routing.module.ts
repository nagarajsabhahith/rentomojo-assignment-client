import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { AuthorizeComponent } from './pages/authorize/authorize.component';
import { CommentsComponent } from './pages/comments/comments.component';
import { CommentEditComponent } from './pages/comment-edit/comment-edit.component';
import { TimeAgoPipe } from 'time-ago-pipe';
import { RepliesComponent } from './pages/replies/replies.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommentReplyComponent } from './pages/comment-reply/comment-reply.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login'
  },
  { path: 'login', component: LoginComponent },
  { path: 'authorize', component: AuthorizeComponent },
  { path: 'comments', component: CommentsComponent },
  { path: 'reply', component: CommentReplyComponent },
  { path: 'update', component: CommentEditComponent },
];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule],
  declarations: [
    LoginComponent,
    AuthorizeComponent,
    CommentsComponent,
    CommentEditComponent,
    TimeAgoPipe,
    RepliesComponent,
    CommentReplyComponent
  ]
})
export class AppRoutingModule { }
