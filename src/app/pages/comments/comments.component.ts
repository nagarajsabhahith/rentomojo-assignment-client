import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  isCommentsLoaded = false;
  comments = [];
  commentForm: FormGroup;
  newCommentAdding = false;
  constructor(
    private api: ApiService,
    public formBuilder: FormBuilder
  ) {
    this.commentForm = this.formBuilder.group({
      comment: [null, Validators.compose([
        Validators.required
      ])],
      parent_id: [null]
    });
  }

  ngOnInit() {
    this.loadComments();
  }

  loadComments() {
    this.isCommentsLoaded = false;
    this.comments = [];
    this.api.Comments().subscribe((res: any) => {
      this.isCommentsLoaded = true;
      this.comments = res.body.comments;
    }, (err: any) => {
    });
  }

  addComment(value: any) {
    this.newCommentAdding = true;
    console.log(value);
    this.api.AddComment(value).subscribe((res: any) => {
      this.newCommentAdding = false;
      this.commentForm.reset();
      this.loadComments();
    }, (err: any) => {});
  }

}
