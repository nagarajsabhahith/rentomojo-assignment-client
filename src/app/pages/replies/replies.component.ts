import { Component, OnInit, Input } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-replies',
  templateUrl: './replies.component.html',
  styleUrls: ['./replies.component.scss']
})
export class RepliesComponent implements OnInit {
  @Input() comments;
  constructor(
    private geneneralService: GeneralService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  replyToComment(comment: any) {
    console.log(comment);
    this.geneneralService.Comment.set(comment);
    this.router.navigate(['/reply']);

  }

  editComment(comment: any) {
    console.log(comment);
    this.geneneralService.Comment.set(comment);
    this.router.navigate(['/update']);
  }

}
