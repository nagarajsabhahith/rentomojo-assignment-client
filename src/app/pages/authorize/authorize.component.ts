import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { GeneralService } from 'src/app/services/general.service';
declare var OAuth: any;
@Component({
  selector: 'app-authorize',
  templateUrl: './authorize.component.html',
  styleUrls: ['./authorize.component.scss']
})
export class AuthorizeComponent implements OnInit {
  constructor(
    private router: Router,
    private api: ApiService,
    private generalService: GeneralService
  ) { }

  ngOnInit() {
    OAuth.initialize('R1HsrKz9hbOi0zZltfjIqwnmjFU');
    OAuth.callback('github').done((res: any) => {
      console.log(res);
      res.me().then((data: any) => {
        console.log(data);

        const params = {
          id: data.id,
          name: data.name,
          alias: data.alias,
          email: data.email,
          access_token: res.access_token,
          avatar: (data.avatar) ? data.avatar : null
        };
        this.api.Login(params).subscribe((result: any) => {
          console.log(result);
          this.generalService.Token.set(result.body.token);
          this.generalService.User.set(result.body.user);
          this.router.navigate(['/comments']);
        }, (err: any) => {
          console.log(err);
        });


      });
    }).fail((err: any) => {
      console.log(err);
    });
  }

}
