import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-comment-edit',
  templateUrl: './comment-edit.component.html',
  styleUrls: ['./comment-edit.component.scss']
})
export class CommentEditComponent implements OnInit {
  comment: any;
  commentForm: FormGroup;
  newCommentEditing = false;
  constructor(
    private generalService: GeneralService,
    public formBuilder: FormBuilder,
    private api: ApiService,
    private router: Router
  ) {
    this.commentForm = this.formBuilder.group({
      comment: [null, Validators.compose([
        Validators.required
      ])],
      _id: [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  ngOnInit() {
    this.comment = this.generalService.Comment.get();
    console.log(this.comment);
    if (this.comment && this.comment._id) {
      this.commentForm.controls._id.setValue(this.comment._id);
      this.commentForm.controls.comment.setValue(this.comment.comment);
    } else {
      alert('Page got refreshed');
      this.router.navigate(['/comments']);
    }
  }

  updateComment(value: any) {
    this.newCommentEditing = true;
    this.api.UpdateComment(value).subscribe((res: any) => {
      this.newCommentEditing = false;
      this.commentForm.reset();
      this.router.navigate(['/comments']);
    }, (err: any) => {
      this.router.navigate(['/comments']);
    });
  }

}
