import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/services/general.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comment-reply',
  templateUrl: './comment-reply.component.html',
  styleUrls: ['./comment-reply.component.scss']
})
export class CommentReplyComponent implements OnInit {
  comment: any;
  commentForm: FormGroup;
  newCommentRepling = false;
  constructor(
    private generalService: GeneralService,
    public formBuilder: FormBuilder,
    private api: ApiService,
    private router: Router
  ) {
    this.commentForm = this.formBuilder.group({
      comment: [null, Validators.compose([
        Validators.required
      ])],
      parent_id: [null, Validators.compose([
        Validators.required
      ])]
    });
  }

  ngOnInit() {
    this.comment = this.generalService.Comment.get();
    console.log(this.comment);
    if (this.comment && this.comment._id) {
      this.commentForm.controls.parent_id.setValue(this.comment._id);
    } else {
      alert('Page got refreshed');
      this.router.navigate(['/comments']);
    }
  }

  replyToComment(value: any) {
    this.newCommentRepling = true;
    this.api.AddComment(value).subscribe((res: any) => {
      this.newCommentRepling = false;
      this.commentForm.reset();
      this.router.navigate(['/comments']);
    }, (err: any) => {
      this.router.navigate(['/comments']);
    });
  }

}
