import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { GeneralService } from './general.service';
@Injectable({
  providedIn: 'root'
})
export class HttpService implements HttpInterceptor {

  constructor(
    private generalService: GeneralService,
    private router: Router,
  ) { }


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token = this.generalService.Token.get();
    const newRequest = req.clone({
      url: environment.endpoint + req.url,
      headers: req.headers.set(
        'x-access-token', `${token}`
      )
    });
    return next.handle(newRequest).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          // do nothing for now
          console.log(event.body);
        }
        return event;
      }),
      catchError((error: any) => {
        const status = error.status;
        console.log(error.error);
        alert(error.error.message);
        if (status === 401) {
          // redirect to the login route
          // or show a modal
          this.generalService.ClearStorage();
          this.router.navigate(['/login']);
        }
        return throwError(error.error);
      }),
    );

  }
}
