import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  commentItem: any;
  constructor(
    private storageService: LocalStorageService,
  ) { }


  public get Token() {
    return {
      set: (token: string) => {
        return this.Storage.set('token', token);
      },
      get: () => {
        const token = this.Storage.get('token');
        return (token) ? token : null;
      }
    };
  }

  public get User() {
    return {
      set: (user: any) => {
        return this.Storage.set('user', user);
      },
      get: () => {
        const user = this.Storage.get('user');
        return (user) ? user : null;
      },
    };
  }

  public get Comment() {
    return {
      set: (item: any) => {
        this.commentItem = item;
      },
      get: () => {
        return this.commentItem;
      }
    };
  }

  private get Storage() {
    return {
      set: (key: string, value: any) => {
        return this.storageService.store(key, value);
      },
      get: (key: string) => {
        return this.storageService.retrieve(key);
      },
      remove: (key: string) => {
        return this.storageService.clear(key);
      },
      clear: () => {
        return this.storageService.clear();
      },
    };
  }

  ClearStorage() {
    return this.Storage.clear();
  }
}
