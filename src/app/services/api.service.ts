import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
  ) { }


  Login(data: any) {
    return this.http.post('/auth/login', data);
  }

  Comments() {
    return this.http.get('/comment/list');
  }

  AddComment(data: any) {
    return this.http.post('/comment/add', data);
  }

  UpdateComment(data: any) {
    return this.http.post('/comment/update', data);
  }

}
